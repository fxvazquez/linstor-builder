all: rocky8 centos7

rocky8:
	EL/build_rocky8.sh

centos7:
	EL/build_centos7.sh

clean:
	rm -rf EL/packages