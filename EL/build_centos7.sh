#!/bin/bash

SCRIPT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )"

# Prebuiliding a docker container with dependencies
docker image build -t centos7_builder:latest --file ${SCRIPT_DIR}/Dockerfile.centos7 ${SCRIPT_DIR}

# Create dir to store the packages
mkdir -p ${SCRIPT_DIR}/packages/

chmod +x ${SCRIPT_DIR}/scripts/builder.sh
docker run \
        --interactive --tty --rm \
        --name centos7_builder \
        --hostname centos7_builder \
        --volume ${SCRIPT_DIR}/scripts/builder.sh:/builder.sh \
        --volume ${SCRIPT_DIR}/packages:/packages \
        --env-file ${SCRIPT_DIR}/../config.env \
        --env DISTRIBUTION_VERSION=EL7 \
        --entrypoint /builder.sh \
    centos7_builder:latest