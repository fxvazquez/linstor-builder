#!/bin/bash

echo "Building Linstor on $(cat /etc/redhat-release)"
echo ""
echo "LINSTOR_SERVER_VERSION: ${LINSTOR_SERVER_VERSION}"
echo "LINSTOR_SERVER_REPO: ${LINSTOR_SERVER_REPO}"
echo "LINSTOR_DOWNLOADS: ${LINSTOR_DOWNLOADS}"
echo ""

LINSTOR_DIRNAME=linstor-server-${LINSTOR_SERVER_VERSION}

# link gradle to gradle wrapper in user's home, because of reasons.
ln -sf ${HOME}/${LINSTOR_DIRNAME}/gradlew /usr/local/bin/gradle

mkdir -p ${HOME}/rpmbuild/SOURCES
mkdir -p ${HOME}/rpmbuild/RPMS/noarch

#
# I was unable to build this from the git sources.
#
#git clone ${LINSTOR_SERVER_REPO} ${HOME}/${LINSTOR_DIRNAME}
#cd ${HOME}/${LINSTOR_DIRNAME}
#git checkout v${LINSTOR_SERVER_VERSION}
#echo ""
#git status
#ls -lah ${HOME}/linstor-server-${LINSTOR_SERVER_VERSION}

#
# Building LINSTOR-SERVER from the oficial release packages:
# https://linbit.com/linbit-software-download-page-for-linstor-and-drbd-linux-driver/#linstor
#
# Download package and untar it
#
cd ${HOME}/rpmbuild/SOURCES/
wget ${LINSTOR_DOWNLOADS}/linstor-server-${LINSTOR_SERVER_VERSION}.tar.gz
cd ${HOME}/
tar -xzf ${HOME}/rpmbuild/SOURCES/linstor-server-${LINSTOR_SERVER_VERSION}.tar.gz

# Start graddle daemon
cd ${HOME}/${LINSTOR_DIRNAME}
./gradlew wrapper -PversionOverride=

# build RPM packages
cd ${HOME}/linstor-server-${LINSTOR_SERVER_VERSION}
rpmbuild -bb --define "debug_package %{nil}" linstor.spec

# Copy packages to the bind-mounted packages directory
mkdir -p /packages/${DISTRIBUTION_VERSION}/
cp -a ${HOME}/rpmbuild/RPMS/noarch/*.rpm /packages/${DISTRIBUTION_VERSION}/

#
# Building PYTHON-LINSTOR from the oficial release packages:
# https://linbit.com/linbit-software-download-page-for-linstor-and-drbd-linux-driver/#linstor
#
# Download package and untar it
#
cd ${HOME}
wget ${LINSTOR_DOWNLOADS}/python-linstor-${PYTHON_LINSTOR_VERSION}.tar.gz
cd ${HOME}/
tar -xzf ${HOME}/python-linstor-${PYTHON_LINSTOR_VERSION}.tar.gz
cd ${HOME}/python-linstor-${PYTHON_LINSTOR_VERSION}
make rpm
cp -a dist/*.noarch.rpm /packages/${DISTRIBUTION_VERSION}/

#
# Building LINSTOR-CLIENT from the oficial release packages:
# https://linbit.com/linbit-software-download-page-for-linstor-and-drbd-linux-driver/#linstor
#
# Download package and untar it
#
cd ${HOME}
wget ${LINSTOR_DOWNLOADS}/linstor-client-${LINSTOR_CLIENT_VERSION}.tar.gz
cd ${HOME}/
tar -xzf ${HOME}/linstor-client-${LINSTOR_CLIENT_VERSION}.tar.gz
cd ${HOME}/linstor-client-${LINSTOR_CLIENT_VERSION}
make rpm
cp -a dist/*.noarch.rpm /packages/${DISTRIBUTION_VERSION}/