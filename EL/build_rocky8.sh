#!/bin/bash

SCRIPT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )"

# Prebuiliding a docker container with dependencies
docker image build -t rocky8_builder:latest --file ${SCRIPT_DIR}/Dockerfile.rocky8 ${SCRIPT_DIR}

# Create dir to store the packages
mkdir -p ${SCRIPT_DIR}/packages/

chmod +x ${SCRIPT_DIR}/scripts/builder.sh
docker run \
        --interactive --tty --rm \
        --name rocky8_builder \
        --hostname rocky8_builder \
        --volume ${SCRIPT_DIR}/scripts/builder.sh:/builder.sh \
        --volume ${SCRIPT_DIR}/packages:/packages \
        --env-file ${SCRIPT_DIR}/../config.env \
        --env DISTRIBUTION_VERSION=EL8 \
        --entrypoint /builder.sh \
    rocky8_builder:latest

