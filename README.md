# Linstor builder

Scripts to build [linstor](https://linbit.com) packages in docker containers.

Status. Currently there are builders for the following OS:

- Centos 7
- Rocky Linux 8
- [TODO] Ubuntu
- [TODO] Debian

They will build packages for:

- linstor-controller
- linstor-satellite
- linstor-common
- python-linstor (api-py)
- linstor-client

## Prerequisites

- Linux
- Docker
- Essential build tools (make). Optional, just run the scripts in the distribution folders to build if you don't have make.

## How to build the packages

- Check the available packages in the [linstor download page](https://linbit.com/linbit-software-download-page-for-linstor-and-drbd-linux-driver/#linstor) and configure the versions in the [`config.env`](config.env) file.
- Run `make`. This will build the packages in containers and copy them to the packages directory.

    ```bash
    kvothe@waystone:~/linstor/linstor-builder$ tree EL/packages/
    EL/packages/
    ├── EL7
    │   ├── linstor-client-1.8.0-1.noarch.rpm
    │   ├── linstor-common-1.13.0~rc.1-1.el7.noarch.rpm
    │   ├── linstor-controller-1.13.0~rc.1-1.el7.noarch.rpm
    │   ├── linstor-satellite-1.13.0~rc.1-1.el7.noarch.rpm
    │   └── python-linstor-1.8.0-1.noarch.rpm
    └── EL8
        ├── linstor-client-1.8.0-1.noarch.rpm
        ├── linstor-common-1.13.0~rc.1-1.el8.noarch.rpm
        ├── linstor-controller-1.13.0~rc.1-1.el8.noarch.rpm
        ├── linstor-satellite-1.13.0~rc.1-1.el8.noarch.rpm
        └── python-linstor-1.8.0-1.noarch.rpm

    2 directories, 10 files
    ```

## Questions nobody asked:

**Will you maintain and update this repo?:**

Probably not. Feel free to fork and modify it to your needs.

**Are the packages generated with this tool production ready?:**

Let me answer with another question, *Are you production ready?*. Seriously, make your own tests, nobody can answer that for you. The source comes from Linbit, either from the [packages page](https://linbit.com/linbit-software-download-page-for-linstor-and-drbd-linux-driver/) or from the [git repositories](https://github.com/LINBIT). The scripts don't make any changes to the code and the packages are built using the methods provides by the developers. 


[Linbit](https://linbit.com/) offer professional services based on these tools that include certified packages. Consider [contacting](https://linbit.com/contact-us/) them.

**Why is this rubbish released under the poisonous terms of the GPLv3 license?:**

 GPL is the only licence I've ever read and I don't want to spend brain cicles reading other ones. So, either was that or public domain. I decided flipping a coin.

**When will be the debian packages available?:**

I don't know. It's not urgent because [Linbit](https://linbit.com/) maintains a debian/ubuntu [repository](https://launchpad.net/~linbit/+archive/ubuntu/linbit-drbd9-stack).

**The build scripts don't build drdb, drdb-utils and other tools but whithout them linstor is useless:**

That's true.

Install [ELREPO](http://elrepo.org/tiki/HomePage) in the Enterprise Linux distribution you use and install them from there.

If you are using a Debian/Ubuntu distro, use the [repository](https://launchpad.net/~linbit/+archive/ubuntu/linbit-drbd9-stack) mentioned above.